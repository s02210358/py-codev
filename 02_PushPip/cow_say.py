import cowsay
import argparse
import sys

parser = argparse.ArgumentParser()

parser.add_argument("message", nargs='*', help='text, that will cow say')
parser.add_argument("-e", default='oo', dest='eyes', metavar='eye_string',
                    help='select the appearance of the cow\'s eyes \
                    (first two characters of the argument string eye_string will be used)')
parser.add_argument("-f", dest='cowfile',
                    help='specifies a cow picture file (''cowfile'') to use')
parser.add_argument("-l", action='store_true',
                    help='list of available builtin cows')
parser.add_argument("-n", action='store_false',
                    help='allow to use the messages with arbitrary whitespace')
parser.add_argument("-T", default='', dest='tongue', metavar='tongue_string',
                    help='select the appearance of the cow\'s eyes \
                    (first two characters of the argument string tongue_string will be used)')
parser.add_argument("-W", default=40, type=int, dest='width', metavar='column',
                    help='width of the text bubble (default 40)')
group = parser.add_argument_group('Modes').add_mutually_exclusive_group()

for i in ['borg', 'dead', 'greedy', 'paranoia', 'stoned', 'tired', 'wired', 'youthful']:
    group.add_argument(f"-{i[0]}", action='store_true', help=f'set up {i} mode')

args = parser.parse_args()

if args.cowfile:
    if args.cowfile in cowsay.list_cows():
        cow = args.cowfile
        cowfile = None
    else:
        with open(args.cowfile, 'r') as f:
            cowfile = cowsay.read_dot_cow(f)
            cow = 'default'
else:
    cowfile = None
    cow = 'default'

preset = None

for i in "bdgpstwy":
    (preset := i) if getattr(args, i) else None

if args.l:
    print(cowsay.list_cows())
else:
    if args.n:
        message = ' '.join(args.message) if args.message else sys.stdin.read()
        message = ' '.join(message.split())
    else:
        message = sys.stdin.read()

    print(cowsay.cowsay(
        message=message,
        cow=cow,
        preset=preset,
        eyes=args.eyes[0:2],
        tongue=args.tongue[0:2],
        width=args.width,
        cowfile=cowfile))
