import cmd
import threading
import readline
import sys
import socket
import shlex


def read_chat(sock, cmd):
    global compl_say
    global compl_login
    
    while True:
        try:
            msg = sock.recv(4096).rstrip().decode()

            match msg[:2]:
                case "l ":
                    compl_login = msg[2:]
                case "s ":
                    compl_say = msg[2:]
                case _:
                    print(f"\n{msg}!\n{cmd.prompt}{readline.get_line_buffer()}", end="", flush=True)
        except Exception:
            return


class cowsay_cmd(cmd.Cmd):
    prompt = "->>"

    def __init__(self, socket):
        super().__init__()
        self.sock = socket
        
    def do_EOF(self, args):
        return True
    
    def do_who(self, args):
        self.sock.sendall("who\n".encode())
        
    def do_cows(self, args):
        self.sock.sendall("cows\n".encode())

    def do_login(self, args):
        self.sock.sendall(f"login {args}\n".encode())

    def complete_login(self, text, line, begidx, endidx):
        line = shlex.split(line)
        self.sock.sendall("cows login\n".encode())
        res = compl_login[1:-1].split(", ")
        return [i[1:-1] for i in res if (i[1:-1].startswith(text) and (line[-1] == "login" or line[-2] == "login"))]
    
    def do_say(self, args):
        args = shlex.split(args)
        self.sock.sendall(f"say {args[0]} {args[1]}\n".encode())

    def complete_say(self, text, line, begidx, endidx):
        line = shlex.split(line)
        self.sock.sendall("who say\n".encode())
        res = compl_say[1:-1].split(", ")
        return [i[1:-1] for i in res if (i[1:-1].startswith(text) and (line[-1] == 'say' or line[-2] == 'say'))]

    def do_yield(self, args):
        self.sock.sendall(f'yield {args}\n'.encode())

    def do_quit(self, args):
        self.sock.sendall("quit\n".encode())
        print("\n")
        return True

# def spam(cmdline, timeout, count):
#     for i in range(count):
#         time.sleep(timeout)
#         print(f"\nI'm a message № {i}!\n{cmdline.prompt}{readline.get_line_buffer()}", end="", flush=True)

# timer = threading.Thread(target=spam, args=(cmdline, 3, 10))
# timer.start()

host = "localhost" if len(sys.argv) < 2 else sys.argv[1]
port = 1337 if len(sys.argv) < 3 else int(sys.argv[2])
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((host, port))
    cmd = cowsay_cmd(s)
    tread = threading.Thread(target=read_chat, args=[s, cmd]).start()
    cmd.cmdloop()
