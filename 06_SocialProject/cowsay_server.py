import asyncio
import cowsay
import shlex

clients = {}

async def chat(reader, writer):
    writer.write("Please log in. To see available usernames use 'cows' comand\n".encode())
    await writer.drain()

    me = "unknown user"

    while True:
        log_in = asyncio.create_task(reader.readline())
        log_res = await log_in
        res_arr = log_res.decode().strip().split()
        
        match res_arr:
            case ["login", name]:
                if name not in cowsay.list_cows():
                    writer.write("Invalid username\n".encode())
                    await writer.drain()
                    continue
                me = name
                print(f"log in user: {me}")
                break
            case ["who", *tmp]:
                if not clients.keys():
                    writer.write("There's no one here\n".encode())
                    await writer.drain()
                    continue
                if tmp and tmp[0] == 'say':
                    writer.write(f"s {list(clients.keys())}\n".encode())
                    await writer.drain()
                else:
                    writer.write(f"{list(clients.keys())}\n".encode())
                    await writer.drain()
            case ["cows", *tmp]:
                if tmp and tmp[0] == 'login':
                    writer.write(f"l {[i for i in cowsay.list_cows() if i not in clients.keys()]}\n".encode())
                    await writer.drain()
                else:
                    writer.write(f"{[i for i in cowsay.list_cows() if i not in clients.keys()]}\n".encode())
                    await writer.drain()
            case ["quit"]:
                reader.feed_eof()
                await reader.read()
                break
            case _:
                writer.write("Please log in. To see available usernames use 'cows' comand\n".encode())
                await writer.drain()

    clients[me] = asyncio.Queue()
    send = asyncio.create_task(reader.readline())
    receive = asyncio.create_task(clients[me].get())

    while not reader.at_eof():
        done, pending = await asyncio.wait([send, receive], return_when=asyncio.FIRST_COMPLETED)
        for q in done:
            if q is send:
                send = asyncio.create_task(reader.readline())
                text = q.result().decode().strip()

                match shlex.split(text):
                    case ["who", *tmp]:
                        if not clients.keys():
                            writer.write("There's no one here\n".encode())
                            await writer.drain()
                            continue
                        if tmp and tmp[0] == 'say':
                            writer.write(f"s {list(clients.keys())}\n".encode())
                            await writer.drain()
                        else:
                            writer.write(f"{list(clients.keys())}\n".encode())
                            await writer.drain()
                    case ["cows", *tmp]:
                        if tmp and tmp[0] == 'login':
                            writer.write(f"l {[i for i in cowsay.list_cows() if i not in clients.keys()]}\n".encode())
                            await writer.drain()
                        else:
                            writer.write(f"{[i for i in cowsay.list_cows() if i not in clients.keys()]}\n".encode())
                            await writer.drain()
                    case ["quit"]:
                        reader.feed_eof()
                        await reader.read()
                        break
                    case ["say", client_name, message]:
                        if client_name not in clients.keys():
                            writer.write(f"{client_name}: unknown user\n".encode())
                            await writer.drain()
                            continue

                        out = clients[client_name]
                        await out.put(cowsay.cowsay(message, cow=me))
                    case ["yield", message]:
                        for out in clients.values():
                            if out is not clients[me]:
                                await out.put(cowsay.cowsay(message, cow=me))
                    case _:
                        writer.write("Invalid comand\n".encode())
                        await writer.drain()
            elif q is receive:
                receive = asyncio.create_task(clients[me].get())
                writer.write(f"{q.result()}\n".encode())
                await writer.drain()

    send.cancel()
    receive.cancel()
    print(me, "DONE")
    del clients[me]
    writer.close()
    await writer.wait_closed()

async def main():
    server = await asyncio.start_server(chat, '0.0.0.0', 1337)
    async with server:
        await server.serve_forever()

asyncio.run(main())
