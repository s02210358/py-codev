import random
import sys
import urllib.request

def bullscows(guess: str, secret: str) -> (int, int):
    i, bulls, cows = 0, 0, 0

    while i < len(guess) and i < len(secret):
        if guess[i] == secret[i]:
            bulls += 1

        i += 1

    for i in set(secret):
        if i in guess:
            cows += 1

    return (bulls, cows)


def ask(prompt: str, valid: list[str] = None) -> str:
    word = input(prompt)

    if valid != None:
        while word not in valid:
            word = input('Word not in valid list\n' + prompt)

    return word


def inform(format_string: str, bulls: int, cows: int) -> None:
    print(format_string.format(bulls, cows))


def gameplay(ask: callable, inform: callable, words: list[str]) -> int:
    secret = random.choice(words)
    attemps, cows, bulls = 0, 0, 0

    while True:
        attemps += 1
        guess = ask('Input word: ', words)
        bulls, cows = bullscows(guess, secret)
        inform('Bulls: {}, Cows: {}', bulls, cows)

        if bulls == len(secret):
            break

    return attemps

if len(sys.argv) == 1 or sys.argv[1] == '--help' or sys.argv[1] == '-h':
    print('python3 -m bullscows <dictionary> <words_len>')
elif len(sys.argv) > 3:
    print('see python3 -m bullscows --help')
else:
    PATH = sys.argv[1]
    word_len = int(sys.argv[2]) if len(sys.argv) == 3 else 5

    try:
        with urllib.request.urlopen(PATH) as f:
            words = f.read().decode().split()
    except Exception:
        with open(PATH) as f:
            words = f.read().split()

    res = [i for i in words if len(i) == word_len]

    if not res:
        print('No words in dictionary with this <words_len>')
    else:
        print(gameplay(ask, inform, [i for i in words if len(i) == word_len]))

