import shlex
import cowsay
import sys
import cmd
import readline


class cowsay_cmd_line(cmd.Cmd):
    prompt = "cowsay>> "
    intro = 'Welcome to the cowsay shell.   Type help or ? to list commands.\n' 

    def do_EOF(self, args):
        """Ctrl+D to close session"""
        return True

    def do_list_cows(self, args):
        'Display list of available cows'
        print(*cowsay.list_cows())
    def do_make_bubble(self, args):
        if len(args) == 0:
            print("Invalid arguments")
            return

        args = shlex.split(args)
        text = args[0]
        args = args[1:]

        brackets, width, wrap_text = 'cowsay', 40, True

        while args:
            match args:
                case ['-b', ('cowsay' | 'cowthink') as opt, *tmp]:
                    brackets = opt
                case ['-w', opt, *tmp]:
                    try:
                        width = int(opt)
                    except Exception:
                        print("Invalid arguments")
                        return
                case ['--wrap', *tmp]:
                    wrap_text = False
                case _:
                    print("Invalid arguments")
                    return

            args = args[2:]

        print(cowsay.make_bubble(text, brackets=cowsay.THOUGHT_OPTIONS[brackets],\
                width=width, wrap_text=wrap_text))
    def complete_make_bubble(self, text, line, beginx, endidx):
        words = 'cowsay', 'cowthink'
        line = shlex.split(line)
        return [i for i in words if ((line[-2] == '-b' or line[-1] == '-b')\
                and i.startswith(text))]
    def help_make_bubble(self):
        print("make_bubble <message ...> [-w width] [-b cowsay|cowthink] [--wrap] -\
                print text in bubble")
        print("\n[-w width] - width message bubble (default 40)")
        print("[-b cowsay|cowthink] - how edges of bubble will look like")
        print("[--wrap] - make text one line")
    def do_cowsay(self, args):
        if len(args) == 0:
            print("Invalid arguments")
            return

        args = shlex.split(args)
        text = args[0]
        args = args[1:]

        cow, eyes, tongue = 'default', 'oo', '  '

        while args:
            match args:
                case ['-e', param, *tmp]:
                    eyes = param[:2]
                case ['-t', param, *tmp]:
                    tongue = param[:2]
                case ['-c', param, *tmp]:
                    if param not in cowsay.list_cows():
                        print("Unknown cow")
                        return
                    else:
                        cow = param

            args = args[2:]

        print(cowsay.cowsay(text, cow=cow, eyes=eyes, tongue=tongue))

    def complete_cowsay(self, text, line, begidx, endidx):
        eyes = 'ss', 'UU', 'ww', 'TT', 'OO', 'oo'
        tongues = 'UU', 'CJ', 'VV', 'LI'
        arr = shlex.split(line)
        
        if arr[-2] == '-e' or arr[-1] == '-e':
            return [i for i in eyes if i.startswith(text)]
        elif arr[-2] == '-t' or arr[-1] == '-t':
            return [i for i in tongues if i.startswith(text)]
        elif arr[-2] == '-c' or arr[-1] == '-c':
            return [i for i in cowsay.list_cows() if i.startswith(text)]
    def help_cowsay(self):
        print("cowsay <message ...> [-t tongue] [-e eye] [-c name] - cow say message")
        print("\n[-t tongue] - cow tongue (first two characters of the argument string string eye_string will be used)")
        print("[-e eye] - cow eyes (first two characters of the argument string string eye_string will be used)")
        print('[-c cow] - name of cow (see available in list_cows)')

    def do_cowthink(self, args):
        if len(args) == 0:
            print("Invalid arguments")
            return

        args = shlex.split(args)
        text = args[0]
        args = args[1:]

        cow, eyes, tongue = 'default', 'oo', '  '

        while args:
            match args:
                case ['-e', param, *tmp]:
                    eyes = param[:2]
                case ['-t', param, *tmp]:
                    tongue = param[:2]
                case ['-c', param, *tmp]:
                    if param not in cowsay.list_cows():
                        print("Unknown cow")
                        return
                    else:
                        cow = param

            args = args[2:]

        print(cowsay.cowthink(text, cow=cow, eyes=eyes, tongue=tongue))

    def complete_cowthink(self, text, line, begidx, endidx):
        eyes = 'ss', 'UU', 'ww', 'TT', 'OO', 'oo'
        tongues = 'UU', 'CJ', 'VV', 'LI'
        arr = shlex.split(line)
        
        if arr[-2] == '-e' or arr[-1] == '-e':
            return [i for i in eyes if i.startswith(text)]
        elif arr[-2] == '-t' or arr[-1] == '-t':
            return [i for i in tongues if i.startswith(text)]
        elif arr[-2] == '-c' or arr[-1] == '-c':
            return [i for i in cowsay.list_cows() if i.startswith(text)]
    def help_cowthink(self):
        print("cowthink <message ...> [-t tongue] [-e eye] [-c name] - cow think message")
        print("\n[-t tongue] - cow tongue (first two characters of the argument string string eye_string will be used)")
        print("[-e eye] - cow eyes (first two characters of the argument string string eye_string will be used)")
        print('[-c cow] - name of cow (see available in list_cows)')

if __name__ =='__main__':
    cowsay_cmd_line().cmdloop()